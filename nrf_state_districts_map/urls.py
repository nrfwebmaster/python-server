from django.conf.urls.defaults import patterns, include, url

from nrf_state_districts_map.views import *

urlpatterns = patterns('',
    url(r'^map/(?P<state_slug>[\w_-]+)/$', map_state_districts, name='map_state_districts'),
    url(r'^map/(?P<state_slug>[\w_-]+)/data(?P<district_number>\d+).json$', map_state_districts_json, name='map_state_districts_json'),
    url(r'^map/(?P<state_slug>[\w_-]+)/(?P<district_slug>[\w_-]+)/$', map_state_districts, name='map_state_districts_one_selected'),
)