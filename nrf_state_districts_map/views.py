from django.shortcuts import render_to_response, get_object_or_404
from django.http import Http404
from boundaries.models import Boundary

import sys

def map_state_districts(request, state_slug, district_slug = None):
    number_of_districts             = Boundary.objects.filter(slug__startswith = state_slug).count()
    if number_of_districts == 0:
        raise Http404
    districts_stroke_color          = '#0073b7'
    districts_fill_color            = '#ffffff'
    district_highlight_fill_color   = '#ff9300'
    water_color                     = '#4b99e1'
    district_slug                   = district_slug if district_slug == None or len(district_slug) > 1 else '0' + district_slug
    current_boundary                = '' if district_slug == None else state_slug + '-' + district_slug
    zoom                            = 3 if request.GET.get('z') == None else int(request.GET.get('z'))
    center_point                    = request.GET.get('cp', False)
    return render_to_response('state_districts_map.html', { 'number_of_districts' : xrange(number_of_districts),
                                                            'districts_stroke_color' : districts_stroke_color,
                                                            'districts_fill_color' : districts_fill_color,
                                                            'district_highlight_fill_color' : district_highlight_fill_color,
                                                            'water_color' : water_color,
                                                            'state_slug' : state_slug,
                                                            'district_slug' : district_slug,
                                                            'current_boundary' : current_boundary,
                                                            'zoom' : zoom,
                                                            'center_point' : center_point })

def map_state_districts_json(request, state_slug, district_number):
    if len(district_number) == 1:
        district_number = '0' + district_number
    district                       = get_object_or_404(Boundary, slug = (state_slug + "-" + district_number))
    return render_to_response('state_districts_map.json', { 'district' : district,
                                                            'state_slug' : state_slug
                                                          })
